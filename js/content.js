function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
// gets a cookie
function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max) +1);
}
function setup(){
    if(getCookie("balance1") == undefined){
        setCookie("balance1",getRandomInt(3000)*10,"1");
        setCookie("balance2",getRandomInt(3000)*10,"1");
        setCookie("balance3",getRandomInt(3000)*10,"1");
        setCookie("balance4",getRandomInt(3000)*10,"1");
        setCookie("id",getRandomInt(1000)*13,"1");   
    }
    
    document.getElementById("balance1").innerHTML = "$"+formatMoney(getCookie("balance1"));
    document.getElementById("balance2").innerHTML = "$"+formatMoney(getCookie("balance2"));
    document.getElementById("balance3").innerHTML = "$"+formatMoney(getCookie("balance3"));
    document.getElementById("balance4").innerHTML = "$"+formatMoney(getCookie("balance4"));

    document.getElementById("name").innerHTML = " - " + getCookie("user") + "#" + getCookie("id");
}
function signout(){
    setCookie("balance1","0","-1");
    setCookie("balance2","0","-1");
    setCookie("balance3","0","-1");
    setCookie("balance4","0","-1");
    setCookie("id","0","-1");
    document.location.href = "./index.html";
}
function formatMoney(number, decPlaces, decSep, thouSep) {
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces;
    decSep = typeof decSep === "undefined" ? "." : decSep;
    thouSep = typeof thouSep === "undefined" ? "," : thouSep;
    var sign = number < 0 ? "-" : "";
    var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
    var j = (j = i.length) > 3 ? j % 3 : 0;

    return sign + (j ? i.substr(0, j) + thouSep : "") + i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) + (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
}