// set a cookie
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
// gets a cookie
function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}
// checks the Login
function checkLogin(form) {
    document.cookie = "user=" + form.user.value + ";path=/";
    document.cookie = "pin=" + form.pin.value + ";path=/";
    checkCookie();
}
// checks a cookie on submit
function checkCookie() {
    if (getCookie('pin') == "1234") {
        alert('Logged in!');
        document.location.href = "./content.html";
        return false;
    }
}
// formats the money
