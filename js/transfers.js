function transferInternally(doc){
    var accountFrom = doc.transferFrom.value;
    var accountTo = doc.transferTo.value;
    var transferAmount = doc.amountInt.value;
    
    var balanceFrom = getCookie("balance"+accountFrom);
    var balanceTo = getCookie("balance"+accountTo);
    setCookie("balance"+accountFrom, (Number(balanceFrom)-Number(transferAmount)), "1");
    setCookie("balance"+accountTo, (Number(balanceTo)+Number(transferAmount)), "1");
    document.location.href = "./content.html";
    return false;
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
// gets a cookie
function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}
function transferExternally(doc){
    var accountFrom = doc.transferFrom.value;
    var transferAmount = doc.amountInt.value;
    
    var balanceFrom = getCookie("balance"+accountFrom);
    setCookie("balance"+accountFrom, (Number(balanceFrom)-Number(transferAmount)), "1");
    document.location.href = "./content.html";
    return false;
}